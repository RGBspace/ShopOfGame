<!doctype html>
<html lang="ru">



















    <meta charset="UTF-8">

    <title>SHOPofGAME.xyz - МАГАЗИН РАНДОМНЫХ ВЕЩЕЙ</title>

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=2.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/css?family=Didact+Gothic|Noto+Sans:700" rel="stylesheet">
    <link rel="stylesheet" href="../../common.css">
    <link rel="stylesheet" href="../../contentCases.css">



















    <style>
        #preloader{position:fixed;z-index:5;top:0;right:0;bottom:0;left:0;background:#151619;opacity:1;transition:2s ease;}
        #preloader img{position:absolute;top:50%;left:50%;margin:-55px;}
    </style>

    <div id="preloader">
        <img src="../../i/2.svg">
    </div>








    <!-- body -->
    <div id="header">



        <div>
            <img src="../../i/br.svg" class="logo header__logo">
            <br>
            SHOPofGAME
        </div>



        <nav class="header__nav">
            <ul>
                <li>
                    <a>
                        <img src="../../i/bg.png">
                        <br>
                        ГЛАВНАЯ
                    </a>
                <li>
                    <a>
                        <img src="../../i/bh.png">
                        <br>
                        F.A.Q.
                    </a>
                <li>
                    <a>
                        <img src="../../i/bx.png">
                        <br>
                        ГАРАНТИИ
                    </a>
                <li>
                    <a>
                        <img src="../../i/bj.png">
                        <br>
                        МЫ В ВК
                    </a>
            </ul>
        </nav>




        <div class="header__auth">
            <a class="header-auth__first-item header-auth__first-item_auth-steam" href="">
                АВТОРИЗАЦИЯ ЧЕРЕЗ
                <span class="header-auth__label-animated">STEAM</span>
                <img id="header-auth__icon-steam" class="header-auth__icon-social" src="../../i/bk.png">
            </a>
            <a class="header-auth__second-item header-auth__second-item_auth-vk" href="">
                АВТОРИЗАЦИЯ ЧЕРЕЗ
                <span class="header-auth__label-animated">VK</span>
                <img class="header-auth__icon-social" src="../../i/bl.png">
            </a>
        </div>




    </div><!-- END header page section -->








    <div id="live-drop">



            <div id="live-drop__label">
                <p class="live-drop__first-row-label">LIVE-ДРОПЫ</p>
                <p class="live-drop__second-row-label">ТОП ИГРОКОВ</p>
            </div>



            <ul id="live-drop__list">

                <li>
                    <div class="live-drop__shadow"></div>
                    <img src="" class="live-drop__img-win">
                    <div class="live-drop__popup">
                        <img src="" class="live-drop-popup__img-case">
                        <p class="live-drop-popup__user-name"></p>
                        <p class="live-drop-popup__name-thing"></p>
                    </div>
                </li>

            </ul>




    </div><!-- END header__live-drop section -->








<ul>

    <li id="title-current-point">
        <a href="/">Главная</a>
        <span>Запрещенное</span>
    </li>

    <li id="baner-case">
        <ul>
            <li id="baner-case__price_sctn1">89 Р</li>
            <li id="baner-case__img-thing_sctn1"><img src="../../i/b.png"></li>
            <li id="baner-case__btn-auth_sctn1">
                АВТОРИЗУЙТЕСЬ
                <div id="baner-case__btn-auth_hover">АВТОРИЗУЙТЕСЬ</div>
            </li>
        </ul>
    </li>

    <li id="wrap-contains-cases">
        <ul class="inner">
            <li id="title-contains-case">
                СОДЕРЖИМОЕ КЕЙСА:
            </li>
            <li id="contains-case">
                <ul>
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i001.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP7
                            <br>
                            ЧЕРТЯТА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i002.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P250
                            <br>
                            ОХОТНИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i003.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P90
                            <br>
                            СЛЕПОЕ ПЯТНО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i004.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P90
                            <br>
                            CHOPPER
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i005.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P90
                            <br>
                            ВИРУС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i006.png">
                        </div>
                        <div class="contains-case__description first-color">
                            ПП-19 БИЗОН
                            <br>
                            АНТИКВАРИАТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i007.png">
                        </div>
                        <div class="contains-case__description first-color">
                            PP-BIZON
                            <br>
                            СИНЯЯ СТРУЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i008.png">
                        </div>
                        <div class="contains-case__description first-color">
                            ПП-19 БИЗОН
                            <br>
                            ТОПЛИВНЫЙ СТЕРЖЕНЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i009.png">
                        </div>
                        <div class="contains-case__description first-color">
                            ПП-19 БИЗОН
                            <br>
                            ОСИРИС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i010.png">
                        </div>
                        <div class="contains-case__description first-color">
                            РЕВОЛЬВЕР R8
                            <br>
                            REBOOT
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i011.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SAWED-OFF
                            <br>
                            РАЗБОЙНИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i012.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SAWED-OFF
                            <br>
                            LIMELIGHT
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i013.png">
                        </div>
                        <div class="contains-case__description" style="padding-top:7px">
                            SAWED-OFF
                            <br>
                            ПИКСЕЛЬНЫЙ КАМУФЛЯЖ «РЖАВЧИНА»
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i014.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SAWED-OFF
                            <br>
                            БЕЗМЯТЕЖНОСТЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i015.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SCAR-20
                            <br>
                            ИЗУМРУД
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i016.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P250
                            <br>
                            СВЕРХНОВАЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i017.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P250
                            <br>
                            ПЯТНО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i018.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P250
                            <br>
                            ЯДЕРНАЯ УГРОЗА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i019.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP7
                            <br>
                            ОКЕАНСКАЯ ПЕНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i020.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP7
                            <br>
                            ОСОБАЯ ДОСТАВКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i021.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP9
                            <br>
                            БУЛЬДОЗЕР
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i022.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP9
                            <br>
                            ГИПНОЗ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i023.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP9
                            <br>
                            ЖЕЛЕЗНАЯ РОЗА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i024.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP9
                            <br>
                            РУБИНОВЫЙ ЯДОВИТЫЙ ДРОТИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i025.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NEGEV
                            <br>
                            КРИКУН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i026.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NEGEV
                            <br>
                            ПОГРУЗЧИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i027.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NOVA
                            <br>
                            ГРАФИТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i028.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NOVA
                            <br>
                            КАРП КОИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i029.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NOVA
                            <br>
                            ВОСХОДЯЩИЙ ЧЕРЕП
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i030.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P2000
                            <br>
                            ЯНТАРНЫЙ ГРАДИЕНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i031.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P2000
                            <br>
                            ПИСТОЛЕТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i032.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P2000
                            <br>
                            СКОРПИОН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i033.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SG 553
                            <br>
                            ПУЛЬС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i034.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SG 553
                            <br>
                            TIGER MOTH
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i035.png">
                        </div>
                        <div class="contains-case__description first-color">
                            AK-47
                            <br>
                            ORBIT MK01
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i036.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P90
                            <br>
                            DEATH GRIP
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i037.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P250
                            <br>
                            КРАСНЫЙ КАМЕНЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i038.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SSG 08
                            <br>
                            МЁРТВЫЕ ГОЛОВЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i039.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P2000
                            <br>
                            WOODSMAN
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i040.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP9
                            <br>
                            СЛИЗЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i041.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SG 553
                            <br>
                            ФАНТОМ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i042.png">
                        </div>
                        <div class="contains-case__description first-color">
                            CZ75-AUTO
                            <br>
                            ТАКТИКОТИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i043.png">
                        </div>
                        <div class="contains-case__description first-color">
                            UMP-45
                            <br>
                            ОБЛУЧЕНИЕ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i044.png">
                        </div>
                        <div class="contains-case__description first-color">
                            XM1014
                            <br>
                            ЗИГГИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i045.png">
                        </div>
                        <div class="contains-case__description first-color">
                            USP-S
                            <br>
                            CYREX
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i046.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DUAL BERETTAS
                            <br>
                            ROYAL CONSORTS
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i047.png">
                        </div>
                        <div class="contains-case__description first-color">
                            G3SG1
                            <br>
                            STINGER
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i048.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M4A1-S
                            <br>
                            FLASHBACK
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i049.png">
                        </div>
                        <div class="contains-case__description first-color">
                            UMP-45
                            <br>
                            SCAFFOLD
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i050.png">
                        </div>
                        <div class="contains-case__description first-color">
                            XM1014
                            <br>
                            SEASONS
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i051.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GALIL AR
                            <br>
                            CRIMSON TSUNAMI
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i052.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SSG 08
                            <br>
                            ПРИЗРАЧНЫЙ ФАНАТИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i053.png">
                        </div>
                        <div class="contains-case__description first-color">
                            TEC-9
                            <br>
                            ЛАВИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i054.png">
                        </div>
                        <div class="contains-case__description first-color">
                            TEC-9
                            <br>
                            ВОЗВРАЩЕНИЕ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i055.png">
                        </div>
                        <div class="contains-case__description first-color">
                            TEC-9
                            <br>
                            ЧАСТИЦА ТИТАНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i056.png">
                        </div>
                        <div class="contains-case__description first-color">
                            UMP-45
                            <br>
                            ГРАН-ПРИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i057.png">
                        </div>
                        <div class="contains-case__description first-color">
                            USP-S
                            <br>
                            ТЕМНАЯ ВОДА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i058.png">
                        </div>
                        <div class="contains-case__description first-color">
                            USP-S
                            <br>
                            СТРАЖ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i059.png">
                        </div>
                        <div class="contains-case__description first-color">
                            USP-S
                            <br>
                            КАМУФЛЯЖ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i060.png">
                        </div>
                        <div class="contains-case__description first-color">
                            USP-S
                            <br>
                            СЛЕДЫ АСФАЛЬТА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i061.png">
                        </div>
                        <div class="contains-case__description first-color">
                            XM1014
                            <br>
                            ЧЕРНЫЙ ГАЛСТУК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i062.png">
                        </div>
                        <div class="contains-case__description first-color">
                            XM1014
                            <br>
                            РАЙСКИЙ СТРАЖ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i063.png">
                        </div>
                        <div class="contains-case__description first-color">
                            XM1014
                            <br>
                            ГОРЕЛКА ТЕКЛУ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i064.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAC-10
                            <br>
                            LAST DIVE
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i065.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M249
                            <br>
                            EMERALD POISON DART
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i066.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NOVA
                            <br>
                            GILA
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i067.png">
                        </div>
                        <div class="contains-case__description first-color">
                            AK-47
                            <br>
                            СИНИЙ ЛАМИНАТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i068.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DESERT EAGLE
                            <br>
                            НАГА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i069.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DESERT EAGLE
                            <br>
                            ПИЛОТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i070.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DESERT EAGLE
                            <br>
                            БУРЯ НА ЗАКАТЕ 弐
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i071.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DUAL BERETTAS
                            <br>
                            СИНИЙ КВАРЦ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i072.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DUAL BERETTAS
                            <br>
                            ПОДРЫВ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i073.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DUAL BERETTAS
                            <br>
                            ДУЭЛИСТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i074.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DUAL BERETTAS
                            <br>
                            ГЕМОГЛОБИН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i075.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DUAL BERETTAS
                            <br>
                            ПРИЧАЛ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i076.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DUAL BERETTAS
                            <br>
                            ГОРОДСКОЙ ШОК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i077.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FAMAS
                            <br>
                            НЕЙРОННАЯ СЕТЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i078.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FAMAS
                            <br>
                            ПУЛЬС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i079.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FAMAS
                            <br>
                            СЕРЖАНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i080.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FAMAS
                            <br>
                            ИСТРЕБИТЕЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i081.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FAMAS
                            <br>
                            СТИКС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i082.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DESERT EAGLE
                            <br>
                            НАСЛЕДИЕ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i083.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DESERT EAGLE
                            <br>
                            ПИЩАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i084.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DESERT EAGLE
                            <br>
                            DIRECTIVE
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i085.png">
                        </div>
                        <div class="contains-case__description first-color">
                            AK-47
                            <br>
                            ИЗУМРУДНЫЕ ЗАВИТКИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i086.png">
                        </div>
                        <div class="contains-case__description first-color">
                            AK-47
                            <br>
                            ПЕРВЫЙ КЛАСС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i087.png">
                        </div>
                        <div class="contains-case__description first-color">
                            AUG
                            <br>
                            ARISTOCRAT
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i088.png">
                        </div>
                        <div class="contains-case__description first-color">
                            AUG
                            <br>
                            ЗАКРУЧЕННЫЙ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i089.png">
                        </div>
                        <div class="contains-case__description first-color">
                            AWP
                            <br>
                            PHOBOS
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i090.png">
                        </div>
                        <div class="contains-case__description" style="padding-top:7px">
                            AWP
                            <br>
                            ПИКСЕЛЬНЫЙ КАМУФЛЯЖ «РОЗОВЫЙ»
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i091.png">
                        </div>
                        <div class="contains-case__description first-color">
                            AWP
                            <br>
                            ГАДЮКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i092.png">
                        </div>
                        <div class="contains-case__description first-color">
                            AWP
                            <br>
                            БОГ ЧЕРВЕЙ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i093.png">
                        </div>
                        <div class="contains-case__description first-color">
                            CZ75-AUTO
                            <br>
                            ЧАША
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i094.png">
                        </div>
                        <div class="contains-case__description first-color">
                            CZ75-AUTO
                            <br>
                            ПОУЛ-ПОЗИЦИЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i095.png">
                        </div>
                        <div class="contains-case__description first-color">
                            CZ75-AUTO
                            <br>
                            КРАСНЫЙ ЯСТРЕБ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i096.png">
                        </div>
                        <div class="contains-case__description first-color">
                            CZ75-AUTO
                            <br>
                            ЛИСТОВАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i097.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DESERT EAGLE
                            <br>
                            ПЛАМЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i098.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DESERT EAGLE
                            <br>
                            КРОВАВАЯ ПАУТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i099.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FAMAS
                            <br>
                            ВАЛЕНТНОСТЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i100.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FIVE-SEVEN
                            <br>
                            ПОВЕРХНОСТНАЯ ЗАКАЛКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i101.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M4A1-S
                            <br>
                            ТЕМНАЯ ВОДА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i102.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M4A1-S
                            <br>
                            ПАДЕНИЕ ИКАРА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i103.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M4A1-S
                            <br>
                            НИТРО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i104.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M4A4
                            <br>
                            РАССВЕТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i105.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M4A4
                            <br>
                            ЗЛОБНЫЙ ДАЙМЕ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i106.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M4A4
                            <br>
                            ГРИФОН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i107.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M4A4
                            <br>
                            СОВРЕМЕННЫЙ ОХОТНИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i108.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M4A4
                            <br>
                            ЗІРКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i109.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAC-10
                            <br>
                            ГРАВИРОВКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i110.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAC-10
                            <br>
                            ЖАР
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i111.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAC-10
                            <br>
                            МАЛАХИТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i112.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAC-10
                            <br>
                            КЛОЧЬЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i113.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAG-7
                            <br>
                            БУЛЬДОЗЕР
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i114.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAG-7
                            <br>
                            ЖАР
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i115.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M4A1-S
                            <br>
                            ЧИСТАЯ ВОДА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i116.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M4A1-S
                            <br>
                            ВАСИЛИСК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i117.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M249
                            <br>
                            КОСМИЧЕСКИЙ ВОИТЕЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i118.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FIVE-SEVEN
                            <br>
                            МЕДНАЯ ГАЛАКТИКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i119.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FIVE-SEVEN
                            <br>
                            НЕОНОВОЕ КИМОНО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i120.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FIVE-SEVEN
                            <br>
                            ДАНЬ ПРОШЛОМУ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i121.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FIVE-SEVEN
                            <br>
                            ТРИУМВИРАТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i122.png">
                        </div>
                        <div class="contains-case__description first-color">
                            G3SG1
                            <br>
                            ХРОНОС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i123.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GALIL AR
                            <br>
                            ПЕРЕСТРЕЛКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i124.png">
                        </div>
                        <div class="contains-case__description" style="padding-top:7px">
                            GALIL AR
                            <br>
                            ПИКСЕЛЬНЫЙ КАМУФЛЯЖ «РЖАВЧИНА»
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i125.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GALIL AR
                            <br>
                            НЕВОЗМУТИМОСТЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i126.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GLOCK-18
                            <br>
                            ЛАТУНЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i127.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GLOCK-18
                            <br>
                            ТАТУИРОВКА ДРАКОНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i128.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GLOCK-18
                            <br>
                            ГРАДИЕНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i129.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GLOCK-18
                            <br>
                            ЖЕРНОВ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i130.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GLOCK-18
                            <br>
                            КОРОЛЕВСКИЙ ЛЕГИОН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i131.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GLOCK-18
                            <br>
                            РЖАВАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i132.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAG-7
                            <br>
                            PRAETORIAN
                        </div>
                    </li><!-- END item contains-case -->
                </ul>
            </li> <!-- END contains-case -->

        </ul>
    </li> <!-- END wrap-contains-cases -->

</ul>








    <ul class="stats">
        <li>
            <p class="counter-value open-cases__value">1000000</p>
            ОТКРЫТО КЕЙСОВ
        </li>
        <li class="users">
            <p class="counter-value users__value">500</p>
            ПОЛЬЗОВАТЕЛЕЙ
        </li>
        <li class="online">
            <p class="counter-value online__value">5000</p>
            ОНЛАЙН
        </li>
    </ul>








    <div class="footer">
        <div class="inner">
            <img src="../../i/bs.svg" class="footer__logo">
            <br>
            <div class="footer__copyright">
                <b class="footer-copyright__title">
                    &#169; 2013-2018 <span>SHOPofGAME</span> - МАГАЗИН СЛУЧАЙНЫХ ВЕЩЕЙ
                </b>
                <p class="footer-copyright__about-site">
                    На данном сайте вы можете открыть различные кейсы<br>
                    CS:GO по самым выгодням ценам. Отправка выигрыша происходит<br>
                    в автоматическом режиме посредством ботов Steam<br>
                </p>
            </div>
            <div class="footer__link">
                Техническая поддрежка:<br>
                support@shopofgame.xyz<br>
                <a href="/terms-of-use">Пользовательское соглашение</a>
            </div>
        </div> <!-- END inner -->
    </div> <!-- END footer -->








    <script src="../../script.js"></script>





















</html>
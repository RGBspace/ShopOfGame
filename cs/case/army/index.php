<!doctype html>
<html lang="ru">



















    <meta charset="UTF-8">

    <title>SHOPofGAME.xyz - МАГАЗИН РАНДОМНЫХ ВЕЩЕЙ</title>

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=2.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/css?family=Didact+Gothic|Noto+Sans:700" rel="stylesheet">
    <link rel="stylesheet" href="../../common.css">
    <link rel="stylesheet" href="../../contentCases.css">



















    <style>
        #preloader{position:fixed;z-index:5;top:0;right:0;bottom:0;left:0;background:#151619;opacity:1;transition:2s ease}
        #preloader img{position:absolute;top:50%;left:50%;margin:-55px}
    </style>

    <div id="preloader">
        <img src="../../i/2.svg">
    </div>








    <!-- body -->
    <div id="header">



        <div>
            <img src="../../i/br.svg" class="logo header__logo">
            <br>
            SHOPofGAME
        </div>



        <nav class="header__nav">
            <ul>
                <li>
                    <a>
                        <img src="../../i/bg.png">
                        <br>
                        ГЛАВНАЯ
                    </a>
                <li>
                    <a>
                        <img src="../../i/bh.png">
                        <br>
                        F.A.Q.
                    </a>
                <li>
                    <a>
                        <img src="../../i/bx.png">
                        <br>
                        ОТЗЫВЫ
                    </a>
            </ul>
        </nav>




        <? if(isset($_SESSION['steam_steamid'])) {?>
            <div class="header__auth">
                <div class="header-auth__first-item header-auth__first-item_auth-steam">
                    <img class="avatar" src="<?= $_SESSION['steam_avatar'] ?>">
                    <span class="username"><?= $_SESSION['steam_personaname'] ?></span>
                    <a class="btn-exit" href="/include/steamauth/logout.php"></a>
                </div>
                <a class="header-auth__second-item header-auth__second-item_field-balance">
                    Баланс: <span id="balance"><?= $_SESSION['balance'] ?></span>
                    <span id="rub"> Р</span>
                    <div class="upload-balance-show-popup"></div>
                </a> <!-- END header-auth__second-item -->
            </div> <!-- END header__auth -->
        <?} else {?>
            <div class="header__auth">
                <a class="header-auth__first-item header-auth__first-item_auth-steam" href="?login">
                    АВТОРИЗАЦИЯ ЧЕРЕЗ
                    <span class="header-auth__label-animated">STEAM</span>
                    <img id="header-auth__icon-steam" class="header-auth__icon-social" src="../../i/bk.png">
                </a>
                <a class="header-auth__second-item header-auth__second-item_auth-vk" href="">
                    АВТОРИЗАЦИЯ ЧЕРЕЗ
                    <span class="header-auth__label-animated">VK</span>
                    <img class="header-auth__icon-social" src="../../i/bl.png">
                </a>
            </div>
        <? } ?>




    </div><!-- END header page section -->








    <div id="live-drop">



            <div id="live-drop__label">
                <p class="live-drop__first-row-label">LIVE-ДРОПЫ</p>
            </div>



            <ul id="live-drop__list">

            <? while ($win = $query_win->fetch_assoc()) {?>
                <li>
                    <div class="live-drop__shadow" style="box-shadow: 0 0 28px 25px <?= rand_box_shadow_color() ?>"></div>
                    <img src="<?= $win['patch_to_img_thing'] ?>" class="live-drop__img-win">
                    <div class="live-drop__popup">
                        <img src="<?= $win['patch_to_img_case'] ?>" class="live-drop-popup__img-case">
                        <p class="live-drop-popup__user-name"><?= $win['user_name'] ?></p>
                        <p class="live-drop-popup__name-thing"><?= $win['caption_win_first'] .' | '. $win['caption_win_second'] ?></p>
                    </div>
                </li>
            <? } ?>

            </ul>




    </div><!-- END header__live-drop section -->








<ul>

    <li id="title-current-point"<? if (isset($_SESSION['steam_steamid']) && $_SESSION['balance'] > 19) {echo 'class="title-current-point_auth-mode"';}; ?>>
        <a href="/">Главная</a>
        <span>Армейское</span>
    </li>

    <li id="baner-case">
        <ul>
            <li id="baner-case__price_sctn1">19 Р</li>
            <li id="baner-case__img-thing_sctn1"><img src="../../i/a.png"></li>
            <li id="baner-case__btn-auth_sctn1">
                АВТОРИЗУЙТЕСЬ
                <div id="baner-case__btn-auth_hover">АВТОРИЗУЙТЕСЬ</div>
            </li>
        </ul>
    </li>

    <li id="wrap-contains-cases">
        <ul class="inner">
            <li id="title-contains-case">
                СОДЕРЖИМОЕ КЕЙСА:
            </li>
            <li id="contains-case">
                <ul>
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i001.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SG 553
                            <br>
                            АНОДИРОВАННАЯ СИНЕВА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i002.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SG 553
                            <br>
                            AERIAL
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i003.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SCAR-20
                            <br>
                            OUTBREAK
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i004.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SCAR-20
                            <br>
                            ГРОТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i005.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SCAR-20
                            <br>
                            ЗЕЛЕНЫЙ МОРПЕХ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i006.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SCAR-20
                            <br>
                            КРОВАВАЯ ПАУТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i007.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SAWED-OFF
                            <br>
                            ЙОРИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i008.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SAWED-OFF
                            <br>
                            ОРИГАМИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i009.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SAWED-OFF
                            <br>
                            ПОЛНАЯ ОСТАНОВКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i010.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SG 553
                            <br>
                            АТЛАС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i011.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SG 553
                            <br>
                            ДАМАССКАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i012.png">
                        </div>
                        <div class="contains-case__description first-color">
                            STATTRAK™ GALIL AR
                            <br>
                            ЛЕДЕНЕЦ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i013.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SSG 08
                            <br>
                            КОГТИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i014.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SSG 08
                            <br>
                            НЕКРОМАНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i015.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SSG 08
                            <br>
                            ТЕМНАЯ ВОДА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i016.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SSG 08
                            <br>
                            КИСЛОТНЫЙ ГРАДИЕНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i017.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SSG 08
                            <br>
                            ПУЧИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i018.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SG 553
                            <br>
                            БРЫЗГИ ВОЛНЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i019.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SAWED-OFF
                            <br>
                            ЯНТАРНЫЙ ГРАДИЕНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i020.png">
                        </div>
                        <div class="contains-case__description first-color">
                            РЕВОЛЬВЕР R8
                            <br>
                            КРОВАВАЯ ПАУТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i021.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P250
                            <br>
                            ВАЛЕНТНОСТЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i022.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P250
                            <br>
                            РЖАВАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i023.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P250
                            <br>
                            IRON CLAD
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i024.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P250
                            <br>
                            УЛЕЙ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i025.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P2000
                            <br>
                            СЕРЕБРО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i026.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P2000
                            <br>
                            КРАСНЫЕ ФРАГМЕНТЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i027.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P2000
                            <br>
                            ПУЛЬС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i028.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P2000
                            <br>
                            ОКЕАНСКИЕ МОТИВЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i029.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P2000
                            <br>
                            СЛОНОВАЯ КОСТЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i030.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P250
                            <br>
                            СНЕЖНАЯ МГЛА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i031.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P90
                            <br>
                            ЭЛИТНОЕ СНАРЯЖЕНИЕ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i032.png">
                        </div>
                        <div class="contains-case__description first-color">
                            ПП-19 БИЗОН
                            <br>
                            ЗНАК ВОДЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i033.png">
                        </div>
                        <div class="contains-case__description first-color">
                            ПП-19 БИЗОН
                            <br>
                            ОКЕАНСКАЯ ГЛУБИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i034.png">
                        </div>
                        <div class="contains-case__description first-color">
                            ПП-19 БИЗОН
                            <br>
                            HARVESTER
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i035.png">
                        </div>
                        <div class="contains-case__description first-color">
                            ПП-19 БИЗОН
                            <br>
                            СИНИЕ ПОЛУТОНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i036.png">
                        </div>
                        <div class="contains-case__description first-color">
                           ПП-19 БИЗОН
                            <br>
                            ЛАТУНЬ 
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i037.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P90
                            <br>
                            ДЕМОНТАЖ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i038.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P90
                            <br>
                            МОДУЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i039.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P90
                            <br>
                            GRIM
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i040.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P2000
                            <br>
                            IMPERIAL
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i041.png">
                        </div>
                        <div class="contains-case__description first-color">
                            TEC-9
                            <br>
                            ЛАТУНЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i042.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P2000
                            <br>
                            TURF
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i043.png">
                        </div>
                        <div class="contains-case__description first-color">
                            AUG
                            <br>
                            КВАДРАНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i044.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SAWED-OFF
                            <br>
                            МОРРИС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i045.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAC-10
                            <br>
                            АЛОХА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i046.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FAMAS
                            <br>
                            MACABRE
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i047.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAG-7
                            <br>
                            HARD WATER
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i048.png">
                        </div>
                        <div class="contains-case__description first-color">
                            UMP-45
                            <br>
                            METAL FLOWERS
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i049.png">
                        </div>
                        <div class="contains-case__description first-color">
                            TEC-9
                            <br>
                            CUT OUT
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i050.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M4A1-S
                            <br>
                            BRIEFING
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i051.png">
                        </div>
                        <div class="contains-case__description first-color">
                            USP-S
                            <br>
                            BLUEPRINT
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i052.png">
                        </div>
                        <div class="contains-case__description first-color">
                            G3SG1
                            <br>
                            СЛЕДОПЫТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i053.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GLOCK-18
                            <br>
                            ПРИШЕЛЕЦ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i054.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAG-7
                            <br>
                            SONAR
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i055.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP9
                            <br>
                            SAND SCALE
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i056.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GALIL AR
                            <br>
                            BLACK SAND
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i057.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP7
                            <br>
                            CIRRUS
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i058.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GLOCK-18
                            <br>
                            IRONWORK
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i059.png">
                        </div>
                        <div class="contains-case__description first-color">
                            CZ75-AUTO
                            <br>
                            POLYMER
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i060.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SCAR-20
                            <br>
                            ВИХРЬ ДЖУНГЛЕЙ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i061.png">
                        </div>
                        <div class="contains-case__description first-color">
                            TEC-9
                            <br>
                            ТРЕСНУВШИЙ ОПАЛ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i062.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAC-10
                            <br>
                            ОКЕАНСКИЕ МОТИВЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i063.png">
                        </div>
                        <div class="contains-case__description first-color">
                            XM1014
                            <br>
                            SLIPSTREAM
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i064.png">
                        </div>
                        <div class="contains-case__description first-color">
                            XM1014
                            <br>
                            СКУМБРИЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i065.png">
                        </div>
                        <div class="contains-case__description first-color">
                            UMP-45
                            <br>
                            КАПРАЛ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i066.png">
                        </div>
                        <div class="contains-case__description first-color">
                            UMP-45
                            <br>
                            ГРУДА КОСТЕЙ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i067.png">
                        </div>
                        <div class="contains-case__description first-color">
                            TEC-9
                            <br>
                            ТОКСИЧНОСТЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i068.png">
                        </div>
                        <div class="contains-case__description first-color">
                            TEC-9
                            <br>
                            ПЕСЧАНАЯ БУРЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i069.png">
                        </div>
                        <div class="contains-case__description first-color">
                            TEC-9
                            <br>
                            ОКОСТЕНЕВШИЙ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i070.png">
                        </div>
                        <div class="contains-case__description first-color">
                            TEC-9
                            <br>
                            JAMBIYA
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i071.png">
                        </div>
                        <div class="contains-case__description first-color">
                            TEC-9
                            <br>
                            АЙЗЕК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i072.png">
                        </div>
                        <div class="contains-case__description first-color">
                            UMP-45
                            <br>
                            ЗАБЛУЖДЕНИЕ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i073.png">
                        </div>
                        <div class="contains-case__description first-color">
                            UMP-45
                            <br>
                            ЛАБИРИНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i074.png">
                        </div>
                        <div class="contains-case__description first-color">
                            XM1014
                            <br>
                            КРАСНЫЙ ПИТОН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i075.png">
                        </div>
                        <div class="contains-case__description first-color">
                            XM1014
                            <br>
                            РТУТЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i076.png">
                        </div>
                        <div class="contains-case__description first-color">
                            XM1014
                            <br>
                            КОСТЕМОЛКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i077.png">
                        </div>
                        <div class="contains-case__description first-color">
                            USP-S
                            <br>
                            ЗАКРУЧЕННЫЙ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i078.png">
                        </div>
                        <div class="contains-case__description first-color">
                            USP-S
                            <br>
                            ПРОВОДНИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i079.png">
                        </div>
                        <div class="contains-case__description first-color">
                            USP-S
                            <br>
                            КРОВАВЫЙ ТИГР
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i080.png">
                        </div>
                        <div class="contains-case__description first-color">
                            UMP-45
                            <br>
                            БУНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i081.png">
                        </div>
                        <div class="contains-case__description first-color">
                            UMP-45
                            <br>
                            БУНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i082.png">
                        </div>
                        <div class="contains-case__description first-color">
                            UMP-45
                            <br>
                            БУНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i083.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GLOCK-18
                            <br>
                            СИНЯЯ ТРЕЩИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i084.png">
                        </div>
                        <div class="contains-case__description first-color">
                            G3SG1
                            <br>
                            ЛАЗУРНАЯ ЗЕБРА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i085.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FIVE-SEVEN
                            <br>
                            ГОРОДСКАЯ ОПАСНОСТЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i086.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FAMAS
                            <br>
                            ВЫЖИВШИЙ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i087.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FIVE-SEVEN
                            <br>
                            VIOLENT DAIMYO
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i088.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FIVE-SEVEN
                            <br>
                            ПАСЛЕН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i089.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FIVE-SEVEN
                            <br>
                            СЕРЕБРЯНЫЙ КВАРЦ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i090.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FIVE-SEVEN
                            <br>
                            SCUMBRIA
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i091.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FIVE-SEVEN
                            <br>
                            КАМИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i092.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FAMAS
                            <br>
                            ГЕКСАН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i093.png">
                        </div>
                        <div class="contains-case__description first-color">
                            G3SG1
                            <br>
                            ВИТРАЖ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i094.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GALIL AR
                            <br>
                            СМОКИНГ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i095.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GALIL AR
                            <br>
                            ОСКОЛКИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i096.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GALIL AR
                            <br>
                            ПЕСЧАНАЯ БУРЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i097.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GALIL AR
                            <br>
                            КАМИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i098.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GALIL AR
                            <br>
                            СИНИЙ ТИТАН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i099.png">
                        </div>
                        <div class="contains-case__description first-color">
                            G3SG1
                            <br>
                            ОРАНЖЕВЫЕ ОСКОЛКИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i100.png">
                        </div>
                        <div class="contains-case__description first-color">
                            G3SG1
                            <br>
                            МРАК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i101.png">
                        </div>
                        <div class="contains-case__description first-color">
                            FAMAS
                            <br>
                            СМЕРТЕНОК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i102.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DUAL BERETTAS
                            <br>
                            ДУХОВИКИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i103.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DUAL BERETTAS
                            <br>
                            ПАНТЕРА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i104.png">
                        </div>
                        <div class="contains-case__description first-color">
                            CZ75-AUTO
                            <br>
                            IMPRINT
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i105.png">
                        </div>
                        <div class="contains-case__description first-color">
                            CZ75-AUTO
                            <br>
                            ГЕКСАН
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i106.png">
                        </div>
                        <div class="contains-case__description first-color">
                            CZ75-AUTO
                            <br>
                            КРОВАВАЯ ПАУТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i107.png">
                        </div>
                        <div class="contains-case__description first-color">
                            AUG
                            <br>
                            РИКОШЕТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i108.png">
                        </div>
                        <div class="contains-case__description first-color">
                            AK-47
                            <br>
                            ЭЛИТНОЕ СНАРЯЖЕНИЕ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i109.png">
                        </div>
                        <div class="contains-case__description first-color">
                            CZ75-AUTO
                            <br>
                            ТИГР
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i110.png">
                        </div>
                        <div class="contains-case__description first-color">
                            CZ75-AUTO
                            <br>
                            СМОКИНГ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i111.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DUAL BERETTAS
                            <br>
                            ДРАКОНИЙ ДУЭТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i112.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DUAL BERETTAS
                            <br>
                            КАРТЕЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i113.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DUAL BERETTAS
                            <br>
                            АНОДИРОВАННАЯ СИНЕВА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i114.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DUAL BERETTAS
                            <br>
                            ЧЕРНАЯ ЛИМБА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i115.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DESERT EAGLE
                            <br>
                            ГОРОДСКОЙ ЩЕБЕНЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i116.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DESERT EAGLE
                            <br>
                            МЕТЕОРИТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i117.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DESERT EAGLE
                            <br>
                            ПОСЛАНИЕ КОРИНФЯНАМ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i118.png">
                        </div>
                        <div class="contains-case__description first-color">
                            DESERT EAGLE
                            <br>
                            БРОНЗОВАЯ ДЕКОРАЦИЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i119.png">
                        </div>
                        <div class="contains-case__description first-color">
                            CZ75-AUTO
                            <br>
                            СПИРАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i120.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GLOCK-18
                            <br>
                            ГОРЕЛКА БУНЗЕНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i121.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP9
                            <br>
                            СМЕРТЕЛЬНЫЙ ЯД
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i122.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP9
                            <br>
                            ДРОТИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i123.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP9
                            <br>
                            УТЕЧКА ОТХОДОВ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i124.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP7
                            <br>
                            СНЕЖНАЯ МГЛА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i125.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP7
                            <br>
                            ГОРОДСКАЯ ОПАСНОСТЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i126.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP7
                            <br>
                            АНОДИРОВАННАЯ СИНЕВА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i127.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NEGEV
                            <br>
                            ТРА-ТА-ТА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i128.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NOVA
                            <br>
                            БУРЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i129.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NOVA
                            <br>
                            ЛЕСНИЧИЙ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i130.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NOVA
                            <br>
                            ПРИЗРАЧНЫЙ КАМУФЛЯЖ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i131.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NOVA
                            <br>
                            EXO
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i132.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NEGEV
                            <br>
                            ПОЧВА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i133.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NEGEV
                            <br>
                            БОЕЦ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i134.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NEGEV
                            <br>
                            ПУСТЫННАЯ АТАКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i135.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP9
                            <br>
                            ЗАКАТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i136.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP7
                            <br>
                            БРОНЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i137.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAG-7
                            <br>
                            ОБЪЕМНЫЕ ШЕСТИУГОЛЬНИКИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i138.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAG-7
                            <br>
                            РАЙСКИЙ СТРАЖ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i139.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M4A1-S
                            <br>
                            СМЕШАННЫЙ КАМУФЛЯЖ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i140.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GLOCK-18
                            <br>
                            ПРИЗРАКИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i141.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M249
                            <br>
                            БЛОКИРОВКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i142.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M249
                            <br>
                            МАГМА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i143.png">
                        </div>
                        <div class="contains-case__description first-color">
                            M249
                            <br>
                            ПРИЗРАК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i144.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GLOCK-18
                            <br>
                            ЗАХОРОНЕНИЕ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i145.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAG-7
                            <br>
                            ПОДЖИГАТЕЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i146.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAC-10
                            <br>
                            CARNIVORE
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i147.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAG-7
                            <br>
                            ЯДРО КОБАЛЬТА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i148.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAC-10
                            <br>
                            УЛЬТРАФИОЛЕТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i149.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAC-10
                            <br>
                            ЯДЕРНЫЙ САД
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i150.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAC-10
                            <br>
                            ХРОМАТИКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i151.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAC-10
                            <br>
                            ЛАЗУРНЫЙ ХИЩНИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i152.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GLOCK-18
                            <br>
                            КАРАМЕЛЬНОЕ ЯБЛОКО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i153.png">
                        </div>
                        <div class="contains-case__description first-color">
                            UMP-45
                            <br>
                            ПЛАМЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i154.png">
                        </div>
                        <div class="contains-case__description first-color">
                            TEC-9
                            <br>
                            ТЕРРАСА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i155.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SSG 08
                            <br>
                            ОБЪЕЗД
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i156.png">
                        </div>
                        <div class="contains-case__description first-color">
                            SAWED-OFF
                            <br>
                            ПЕРВЫЙ КЛАСС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i157.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P90
                            <br>
                            АРКТИЧЕСКАЯ СЕТКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i158.png">
                        </div>
                        <div class="contains-case__description first-color">
                            P2000
                            <br>
                            КОЛЬЧУГА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i159.png">
                        </div>
                        <div class="contains-case__description first-color">
                            NOVA
                            <br>
                            ОРАНЖЕВОЕ ПЛАМЯ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i160.png0">
                        </div>
                        <div class="contains-case__description first-color">
                            MP9
                            <br>
                            ЯЩИК ПАНДОРЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i161.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP9
                            <br>
                            ХОТ-РОД
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i162.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MP9
                            <br>
                            ТЕМНЫЙ ВЕК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i163.png">
                        </div>
                        <div class="contains-case__description first-color">
                            MAG-7
                            <br>
                            АНТИТЕРРАСА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i164.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GALIL AR
                            <br>
                            SAGE SPRAY
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i165.png">
                        </div>
                        <div class="contains-case__description first-color">
                            GALIL AR
                            <br>
                            ВОДНАЯ ТЕРРАСА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i166.png">
                        </div>
                        <div class="contains-case__description first-color">
                            CZ75-AUTO
                            <br>
                            НИТРО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i167.png">
                        </div>
                        <div class="contains-case__description first-color">
                            AUG
                            <br>
                            ХОТ-РОД
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i168.png">
                        </div>
                        <div class="contains-case__description first-color">
                            AUG
                            <br>
                            АНОДИРОВАННАЯ СИНЕВА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i169.png">
                        </div>
                        <div class="contains-case__description first-color">
                            XM1014
                            <br>
                            СМЕШАННЫЙ СИНИЙ КАМУФЛЯЖ
                        </div>
                    </li><!-- END item contains-case -->
                </ul>
            </li> <!-- END contains-case -->

        </ul>
    </li> <!-- END wrap-contains-cases -->

</ul>








    <ul class="stats">
        <li>
            <p class="counter-value open-cases__value">1000000</p>
            ОТКРЫТО КЕЙСОВ
        </li>
        <li class="users">
            <p class="counter-value users__value">500</p>
            ПОЛЬЗОВАТЕЛЕЙ
        </li>
        <li class="online">
            <p class="counter-value online__value">5000</p>
            ОНЛАЙН
        </li>
    </ul>








    <div class="footer">
        <div class="inner">
            <img src="../../i/bs.svg" class="footer__logo">
            <br>
            <div class="footer__copyright">
                <b class="footer-copyright__title">
                    &#169; 2013-2018 <span>SHOPofGAME</span> - МАГАЗИН СЛУЧАЙНЫХ ВЕЩЕЙ
                </b>
                <p class="footer-copyright__about-site">
                    На данном сайте вы можете открыть различные кейсы<br>
                    CS:GO по самым выгодням ценам. Отправка выигрыша происходит<br>
                    в автоматическом режиме посредством ботов Steam<br>
                </p>
            </div>
            <div class="footer__link">
                Техническая поддрежка:<br>
                support@shopofgame.xyz<br>
                <a href="/terms-of-use">Пользовательское соглашение</a>
            </div>
        </div> <!-- END inner -->
    </div> <!-- END footer -->








    <script src="../../script.js"></script>





















</html>
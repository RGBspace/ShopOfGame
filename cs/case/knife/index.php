<!doctype html>
<html lang="ru">



















    <meta charset="UTF-8">

    <title>SHOPofGAME.xyz - МАГАЗИН РАНДОМНЫХ ВЕЩЕЙ</title>

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=2.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/css?family=Didact+Gothic|Noto+Sans:700" rel="stylesheet">
    <link rel="stylesheet" href="../../common.css">
    <link rel="stylesheet" href="../../contentCases.css">



















    <style>
        #preloader{position:fixed;z-index:5;top:0;right:0;bottom:0;left:0;background:#151619;opacity:1;transition:2s ease;}
        #preloader img{position:absolute;top:50%;left:50%;margin:-55px;}
    </style>

    <div id="preloader">
        <img src="../../i/2.svg">
    </div>








    <!-- body -->
    <div id="header">



        <div>
            <img src="../../i/br.svg" class="logo header__logo">
            <br>
            SHOPofGAME
        </div>



        <nav class="header__nav">
            <ul>
                <li>
                    <a>
                        <img src="../../i/bg.png">
                        <br>
                        ГЛАВНАЯ
                    </a>
                <li>
                    <a>
                        <img src="../../i/bh.png">
                        <br>
                        F.A.Q.
                    </a>
                <li>
                    <a>
                        <img src="../../i/bx.png">
                        <br>
                        ГАРАНТИИ
                    </a>
                <li>
                    <a>
                        <img src="../../i/bj.png">
                        <br>
                        МЫ В ВК
                    </a>
            </ul>
        </nav>




        <div class="header__auth">
            <a class="header-auth__first-item header-auth__first-item_auth-steam" href="">
                АВТОРИЗАЦИЯ ЧЕРЕЗ
                <span class="header-auth__label-animated">STEAM</span>
                <img id="header-auth__icon-steam" class="header-auth__icon-social" src="../../i/bk.png">
            </a>
            <a class="header-auth__second-item header-auth__second-item_auth-vk" href="">
                АВТОРИЗАЦИЯ ЧЕРЕЗ
                <span class="header-auth__label-animated">VK</span>
                <img class="header-auth__icon-social" src="../../i/bl.png">
            </a>
        </div>




    </div><!-- END header page section -->








    <div id="live-drop">



            <div id="live-drop__label">
                <p class="live-drop__first-row-label">LIVE-ДРОПЫ</p>
                <p class="live-drop__second-row-label">ТОП ИГРОКОВ</p>
            </div>



            <ul id="live-drop__list">

                <li>
                    <div class="live-drop__shadow"></div>
                    <img src="" class="live-drop__img-win">
                    <div class="live-drop__popup">
                        <img src="" class="live-drop-popup__img-case">
                        <p class="live-drop-popup__user-name"></p>
                        <p class="live-drop-popup__name-thing"></p>
                    </div>
                </li>

            </ul>




    </div><!-- END header__live-drop section -->








<ul>

    <li id="title-current-point">
        <a href="/">Главная</a>
        <span>Нож</span>
    </li>

    <li id="baner-case">
        <ul>
            <li id="baner-case__price_sctn1">4989 Р</li>
            <li id="baner-case__img-thing_sctn1"><img src="../../i/e.png"></li>
            <li id="baner-case__btn-auth_sctn1">
                АВТОРИЗУЙТЕСЬ
                <div id="baner-case__btn-auth_hover">АВТОРИЗУЙТЕСЬ</div>
            </li>
        </ul>
    </li>

    <li id="wrap-contains-cases">
        <ul class="inner">
            <li id="title-contains-case">
                СОДЕРЖИМОЕ КЕЙСА:
            </li>
            <li id="contains-case">
                <ul>
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i001.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            ПАТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i002.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            ЧИСТАЯ ВОДА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i003.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            ПОВЕРХНОСТНАЯ ЗАКАЛКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i004.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            КРОВАВАЯ ПАУТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i005.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            ДАМАССКАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i006.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            ВОЛНЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i007.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            АФРИКАНСКАЯ СЕТКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i008.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ОХОТНИЧИЙ НОЖ
                            <br>
                            НОЧЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i009.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ОХОТНИЧИЙ НОЖ
                            <br>
                            АФРИКАНСКАЯ СЕТКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i010.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ M9
                            <br>
                            ГАММА-ВОЛНЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i011.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ M9
                            <br>
                            ПЫЛЬНИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i012.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ M9
                            <br>
                            АФРИКАНСКАЯ СЕТКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i013.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ОХОТНИЧИЙ НОЖ
                            <br>
                            ОЖОГИ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i014.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            СЕВЕРНЫЙ ЛЕС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i015.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ M9
                            <br>
                            ВОРОНЕНАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i016.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            ЗУБ ТИГРА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i017.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ BOWIE KNIFE
                            <br>
                            ПОВЕРХНОСТНАЯ ЗАКАЛКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i018.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ-БАБОЧКА
                            <br>
                            СЕВЕРНЫЙ ЛЕС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i019.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ M9
                            <br>
                            КРОВАВАЯ ПАУТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i020.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ M9
                            <br>
                            ДАМАССКАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i021.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ M9
                            <br>
                            ВОЛНЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i022.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ M9
                            <br>
                            ГРАДИЕНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i023.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            ПЫЛЬНИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i024.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            АФРИКАНСКАЯ СЕТКА

                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i025.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            УБИЙСТВО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i026.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            ПАТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i027.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            УЛЬТРАФИОЛЕТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i028.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ОХОТНИЧИЙ НОЖ
                            <br>
                            УБИЙСТВО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i029.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ОХОТНИЧИЙ НОЖ
                            <br>
                            ГОРОДСКАЯ МАСКИРОВКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i030.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            ВОРОНЕНАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i031.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            GAMMA DOPPLER
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i032.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            LORE
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i033.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            УЛЬТРАФИОЛЕТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i034.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            ДАМАССКАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i035.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ M9
                            <br>
                            FREEHAND
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i036.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ТЫЧКОВЫЕ НОЖИ
                            <br>
                            ГРАДИЕНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i037.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            FREEHAND
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i038.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            GAMMA DOPPLER
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i039.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            МРАМОРНЫЙ ГРАДИЕНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i040.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            ПЫЛЬНИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i041.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            АФРИКАНСКАЯ СЕТКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i042.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            САЖА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i043.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            FREEHAND
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i044.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ SHADOW DAGGERS
                            <br>
                            ГОРОДСКАЯ МАСКИРОВКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i045.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ КЕРАМБИТ
                            <br>
                            ГАММА-ВОЛНЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i046.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ M9
                            <br>
                            ПОВЕРХНОСТНАЯ ЗАКАЛКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i047.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ M9
                            <br>
                            ЗУБ ТИГРА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i048.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ M9
                            <br>
                            УЛЬТРАФИОЛЕТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i049.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ТЫЧКОВЫЕ НОЖИ
                            <br>
                            ВОРОНЕНАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i050.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ SHADOW DAGGERS
                            <br>
                            ПОВЕРХНОСТНАЯ ЗАКАЛКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i051.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ТЫЧКОВЫЕ НОЖИ
                            <br>
                            КРОВАВАЯ ПАУТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i052.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            AUTOTRONIC
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i053.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ТЫЧКОВЫЕ НОЖИ
                            <br>
                            НОЧЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i054.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ SHADOW DAGGERS
                            <br>
                            АФРИКАНСКАЯ СЕТКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i055.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ТЫЧКОВЫЕ НОЖИ
                            <br>
                            УБИЙСТВО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i056.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            НОЧЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i057.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            УБИЙСТВО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i058.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            BLACK LAMINATE
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i059.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ФАЛЬШИОН
                            <br>
                            САЖА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i060.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ФАЛЬШИОН
                            <br>
                            УБИЙСТВО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i061.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ФАЛЬШИОН
                            <br>
                            ПАТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i062.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ФАЛЬШИОН
                            <br>
                            ГОРОДСКАЯ МАСКИРОВКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i063.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ-БАБОЧКА
                            <br>
                            КРОВАВАЯ ПАУТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i064.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ-БАБОЧКА
                            <br>
                            ГРАДИЕНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i065.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ-БАБОЧКА
                            <br>
                            АФРИКАНСКАЯ СЕТКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i066.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ-БАБОЧКА
                            <br>
                            УБИЙСТВО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i067.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ФАЛЬШИОН
                            <br>
                            СЕВЕРНЫЙ ЛЕС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i068.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ФАЛЬШИОН
                            <br>
                            КРОВАВАЯ ПАУТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i069.png">
                        </div>
                        <div class="contains-case__description fourth-color" style="padding-top:7px">
                            ★ BOWIE KNIFE
                            <br>
                            ПИКСЕЛЬНЫЙ КАМУФЛЯЖ «ЛЕС»
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i070.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ BOWIE KNIFE
                            <br>
                            НОЧЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i071.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ФАЛЬШИОН
                            <br>
                            АФРИКАНСКАЯ СЕТКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i072.png">
                        </div>
                        <div class="contains-case__description fourth-color" style="padding-top:7px">
                            ★ ФАЛЬШИОН
                            <br>
                            ПИКСЕЛЬНЫЙ КАМУФЛЯЖ «ЛЕС»
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i073.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            ВОРОНЕНАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i074.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            ЧИСТАЯ ВОДА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i075.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            ПОВЕРХНОСТНАЯ ЗАКАЛКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i076.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            ДАМАССКАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i077.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            ВОЛНЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i078.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            ГРАДИЕНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i079.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            КРОВАВАЯ ПАУТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i080.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            BLACK LAMINATE
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i081.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            ДАМАССКАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i082.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            ВОЛНЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i083.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            ГРАДИЕНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i084.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            ПИКСЕЛЬНЫЙ КАМУФЛЯЖ «ЛЕС»
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i085.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ BOWIE KNIFE
                            <br>
                            САЖА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i086.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ BOWIE KNIFE
                            <br>
                            УБИЙСТВО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i087.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            GAMMA DOPPLER
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i088.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            LORE
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i089.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            МРАМОРНЫЙ ГРАДИЕНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i090.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            ПЫЛЬНИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i091.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            AUTOTRONIC
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i092.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            BLACK LAMINATE
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i093.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            ВОРОНЕНАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i094.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            ЧИСТАЯ ВОДА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i095.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            ПОВЕРХНОСТНАЯ ЗАКАЛКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i096.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            КРОВАВАЯ ПАУТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i097.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            МРАМОРНЫЙ ГРАДИЕНТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i098.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            ПЫЛЬНИК
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i099.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            ВОЛНЫ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i100.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ-БАБОЧКА
                            <br>
                            ВОРОНЕНАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i101.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            ПАТИНА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i102.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            ЗУБ ТИГРА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i103.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            УЛЬТРАФИОЛЕТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i104.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ШТЫК-НОЖ
                            <br>
                            ГОРОДСКАЯ МАСКИРОВКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i105.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ BOWIE KNIFE
                            <br>
                            ВОРОНЕНАЯ СТАЛЬ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i106.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ BOWIE KNIFE
                            <br>
                            СЕВЕРНЫЙ ЛЕС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i107.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            УБИЙСТВО
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i108.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            ЗУБ ТИГРА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i109.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            УЛЬТРАФИОЛЕТ
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i110.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ОХОТНИЧИЙ НОЖ
                            <br>
                            СЕВЕРНЫЙ ЛЕС
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i111.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ ОХОТНИЧИЙ НОЖ
                            <br>
                            ПОВЕРХНОСТНАЯ ЗАКАЛКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i112.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ СКЛАДНОЙ НОЖ
                            <br>
                            АФРИКАНСКАЯ СЕТКА
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i113.png">
                        </div>
                        <div class="contains-case__description fourth-color" style="padding-top:7px">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            ПИКСЕЛЬНЫЙ КАМУФЛЯЖ «ЛЕС»
                        </div>
                    </li><!-- END item contains-case -->
                    <li>
                        <div class="contains-case__wrap-img">
                            <img src="i114.png">
                        </div>
                        <div class="contains-case__description fourth-color">
                            ★ НОЖ С ЛЕЗВИЕМ-КРЮКОМ
                            <br>
                            ГОРОДСКАЯ МАСКИРОВКА
                        </div>
                    </li><!-- END item contains-case -->
                </ul>
            </li> <!-- END contains-case -->

        </ul>
    </li> <!-- END wrap-contains-cases -->

</ul>








    <ul class="stats">
        <li>
            <p class="counter-value open-cases__value">1000000</p>
            ОТКРЫТО КЕЙСОВ
        </li>
        <li class="users">
            <p class="counter-value users__value">500</p>
            ПОЛЬЗОВАТЕЛЕЙ
        </li>
        <li class="online">
            <p class="counter-value online__value">5000</p>
            ОНЛАЙН
        </li>
    </ul>








    <div class="footer">
        <div class="inner">
            <img src="../../i/bs.svg" class="footer__logo">
            <br>
            <div class="footer__copyright">
                <b class="footer-copyright__title">
                    &#169; 2013-2018 <span>SHOPofGAME</span> - МАГАЗИН СЛУЧАЙНЫХ ВЕЩЕЙ
                </b>
                <p class="footer-copyright__about-site">
                    На данном сайте вы можете открыть различные кейсы<br>
                    CS:GO по самым выгодням ценам. Отправка выигрыша происходит<br>
                    в автоматическом режиме посредством ботов Steam<br>
                </p>
            </div>
            <div class="footer__link">
                Техническая поддрежка:<br>
                support@shopofgame.xyz<br>
                <a href="/terms-of-use">Пользовательское соглашение</a>
            </div>
        </div> <!-- END inner -->
    </div> <!-- END footer -->








    <script src="../../script.js"></script>





















</html>
var title = document.getElementsByClassName('list__title'),
    description = document.getElementsByClassName('faq-list__description'),
    i;

for (i = 0; i < description.length; i++ ) {description[i].style.height = 0};

function faqListShowHide(numm) {
    
    if (description[numm].style.height.match(/\d{1,3}/) != 0) {
       description[numm].style.height = 0;
       description[numm].style.padding = '0 18px';
    } else {
        description[numm].style.padding = '10px 18px';
        switch (numm) {
            case 0:
                description[numm].style.height = '62px';
                break;
            case 1:
                description[numm].style.height = '83px';
                break;
            case 2:
                description[numm].style.height = '62px';
                break;
            case 3:
                description[numm].style.height = '62px';
                break;
            case 4:
                description[numm].style.height = '62px';
                break;
            case 5:
                description[numm].style.height = '50px';
                break;
            case 6:
                description[numm].style.height = '83px';
                break;
            case 7:
                description[numm].style.height = '219px';
                break;
            case 8:
                description[numm].style.height = '62px';
                break;
            case 9:
                description[numm].style.height = '50px';
                break;

       }// END switch
    }// END condition

}// END func

